# Alternatives for Afivo

# Related projects

* [afivo-streamer](https://gitlab.com/MD-CWI-NL/afivo-streamer): A collection of
  2D/3D streamer models based on Afivo

# Alternative frameworks

* [Boxlib](https://ccse.lbl.gov/BoxLib/index.html)
* [Gerris](http://gfs.sourceforge.net/wiki/index.php/Main_Page)
* [Ramses](http://www.ics.uzh.ch/~teyssier/ramses/RAMSES.html)
* [Paramesh](https://sourceforge.net/projects/paramesh/)
* [Chombo](https://commons.lbl.gov/display/chombo/Chombo+-+Software+for+Adaptive+Solutions+of+Partial+Differential+Equations)
